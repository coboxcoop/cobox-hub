const crypto = require('cobox-crypto')
const express = require('express')
const constants = require('cobox-constants')
const bodyParser = require('body-parser')
const mkdirp = require('mkdirp')
const path = require('path')
const debug = require('debug')('cobox-hub')
const { loadKey, saveKey } = require('cobox-keys')
const sodium = require('sodium-native')
const assert = require('assert')
const cors = require('cors')
const capture = require('capture-exit')
const { untilde } = require('./util')

capture.captureExit()

const start = require('./lib/start')
const close = require('./lib/close')
const Routes = require('./app/routes')
const Controllers = require('./app/controllers')
const SuperuserStore = require('./lib/superusers')
const ReplicatorStore = require('./lib/replicators')

const PARENT_KEY = 'parent_key'
const { keyIds } = constants
const { hex } = require('./util')

function Application (opts = {}) {
  const _id = hex(crypto.randomBytes(2))
  const storage = opts.storage = untilde(opts.storage) || constants.hubStorage
  const udpPort = opts.udpPort

  mkdirp.sync(path.join(storage))

  const parentKey = loadKey(storage, PARENT_KEY) || crypto.masterKey()
  saveKey(storage, PARENT_KEY, parentKey)
  const identity = crypto.deriveBoxKeyPair(parentKey, keyIds.identity)
  sodium.sodium_memzero(parentKey)

  const app = express()

  const shutdown = Shutdown(app)
  const api = {
    superusers: SuperuserStore(storage, { identity }),
    replicators: ReplicatorStore(storage),
    store: defaultStore({}),
    settings: { storage },
    profile: identity,
    udpPort,
    shutdown
  }

  api.controllers = Controllers(api)
  app.use(bodyParser.urlencoded({ extended: true }))
  app.use(bodyParser.json())
  app.use(cors())
  app.use('/api', Routes(api))
  app.set('api', api)

  app.start = api.start = start(app, opts)
  app.close = api.close = close(app, opts)

  capture.onExit(shutdown)

  function Shutdown (app) {
    return function gracefulShutdown () {
      return new Promise((resolve, reject) => {
        app.close((err, done) => {
          if (err) return reject(err, done)
          resolve(done)
        })
      })
    }
  }

  return app
}

function defaultStore (opts = {}) {
  return {
    commands: {
      announcements: {},
      replicates: {}
    }
  }
}

module.exports = Application
