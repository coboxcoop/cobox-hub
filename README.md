<div align="center">
  <img src="https://cobox.cloud/src/svg/logo.svg">
</div>

# Seeder
## Table of Contents

* [About](#about)
* [Installing and Updating](#installing-and-updating)
  * [Install & Update Script](#install-and-update-script)
  * [Troubleshooting on Linux](#troubleshooting-on-linux)
  * [Verify Installation](#verify-installation)
  * [NPM Install](#npm-install)
  * [Git Install](#git-install)
* [Usage](#usage)
* [Concepts](#concepts)
* [Docs](#docs)
* [Development](#development)
* [Contributing](#contributing)
* [License](#license)

## About

CoBox is built on Dat. Dat is a modular peer-to-peer technology stack. You can find a good explanation of how it works in the guide ['how Dat works’](https://datprotocol.github.io/how-dat-works/), and you can find out how we’ve made use of Dat in [our developer docs](https://develop.cobox.cloud/core_technologies.html).

For CoBox, we’ve used Dat to build fully encrypted private spaces that synchronise seamlessly across multiple devices. This means you, your friends and colleagues no longer have to rely on corporate servers to store your files, you can hold onto them for each-other.

CoBox Seeder is an 'always on' blind replicator device designed to be hosted in your office or home. It exposes an ExpressJS HTTP JSON API, pairing with [CoBox Server](https://gitlab.com/coboxcoop/server) using UDP packets, and a Yargs CLI over the LAN, as well as processing remote commands from a registered and authenticated CoBox Server instance over the [Hyperswarm DHT](https://github.com/hyperswarm/hyperswarm) using the [Hypercore Protocol](https://github.com/mafintosh/hypercore-protocol/).

This package makes use of [Superusers](https://gitlab.com/coboxcoop/superusers), as well as storing multiple [Replicator](https://gitlab.com/coboxcoop/replicator) instances.

## Installing and Updating

### Install and Update Script

To install or update cobox-server, you should run the [install script](). To do that, you may either download and run the script manually, or use the following cURL or Wget command:

```
curl -o- https://cobox.cloud/releases/cobox-seeder_v1.0.0-alpha1/download.sh | bash
```

```
wget -qO- https://cobox.cloud/releases/cobox-seeder_v1.0.0-alpha1/download.sh | bash
```

Running either of the above commands downloads a script and runs it. The script downloads a tarball that contains the release binary required by your operating system with additional assets and unpacks the contents to the install directory.

### Troubleshooting on Linux

On Linux, after running the install script, if you get `cobox-seeder: command not found` or see no feedback from your terminal after you type `command -v cobox-seeder`, simply close your current terminal, open a new terminal, and try verifying again.

### Verify Installation

To verify that CoBox has been installed correctly, do:

```
command -v cobox-seeder
```

### NPM Install

```
npm install -g @coboxcoop/seeder

cobox-seeder
```

### Git Install

Before you get started, ensure you have `git` and `node` installed. We currently release using `v12.16.3` which we recommend using. You can download and install nodejs from your distribution package manager, or [direct from their website](https://nodejs.org/en/download/).

```
# clone the repository
git clone http://gitlab.com/coboxcoop/server && cd server

npm install -g yarn

# install dependencies
yarn

# start the app
yarn start

# use the cli
node bin
```

## Usage

CoBox Seeder can be administrated in two ways.

1. Directly on the device that is running it using the CLI
2. By sending commands across the hypercore-protocol from an authenticated CoBox Server instance

### CLI

```
CoBox Hub 1.0 - a tool for distributed and mutual backup infrastructure

  Copyright (C) 2019 Magma Collective T&DT, License GNU AGPL v3+
  This is free software: you are free to change and redistribute it
  For the latest sourcecode go to <https://code.cobox.cloud/>

Usage: bin <command> [options]

Commands:
  bin backups <command>    back-up your peers' spaces
  bin broadcast <command>  announce authentication details over the local area
                           network
  bin keys <command>       manage your hub's keys
  bin down [options]       stop the seeder app
  bin up [options]         start the seeder app

Options:
  -h, --help, --help        Show help                                  [boolean]
  -v, --version, --version  Show version number                        [boolean]

For more information on cobox read the manual: man cobox-hub
Please report bugs on <http://gitlab.com/coboxcoop/core/issues>.
```

### Commands

When first initialised, CoBox Seeder begins broadcasting UDP packets announcing its authentication keys. You can use [CoBox Server](https://gitlab.com/coboxcoop/server) to retrieve these keys over the LAN, either with the `cobox seeders pair` CLI command or through the admin dashboard in the UI. With these keys you can send commands to the Seeder app and request it to perform backups. See the [CoBox CLI](https://gitlab.com/coboxcoop/server) for more details.

# Concepts

To be written

# Docs

To be written

# Development

If you want to develop, we recommend using [Node Version Manager](https://github.com/nvm-sh/nvm), [Yarn](https://yarnpkg.com/) and we also suggest you check out [our monorepo](https://gitlab.com/coboxcoop/core) which makes use of yarn workspaces and git modules to help ease the development of multiple cross-dependent packages. The mono-repo contains [instructions](https://gitlab.com/coboxcoop/core/-/blob/master/DEVELOPMENT.md) on how to get setup to develop and extend the CoBox APIs, as well as some resources on how to use the tools we make use of and make merge requests.

# Contributing

To be written

# License

[`AGPL-3.0-or-later`](./LICENSE)
