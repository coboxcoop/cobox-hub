const { isString } = require('util')
const os = require('os')
const path = require('path')
const chalk = require('chalk')
const { DEFAULT_COLOUR } = require('./bin/lib/util')
const { printBanner } = require('cobox-hub-cli/util')

function printAppInfo (opts) {
  const { hostname, port, udpPort, storage } = opts

  printBanner()

  console.log()
  console.log(`listening on ${chalk.hex(DEFAULT_COLOUR)(`http://${hostname}:${port}`)}`)
  console.log(`pairing on ${chalk.hex(DEFAULT_COLOUR)(`http://${hostname}:${udpPort}`)}`)
  console.log(`storage at ${chalk.hex(DEFAULT_COLOUR)(`file://${untilde(storage)}`)}`)
  console.log()
}

function untilde (str) {
  if (str[0] === '~') return path.join(os.homedir(), str.slice(1))
  return str
}

function hex (buf) {
  if (Buffer.isBuffer(buf)) return buf.toString('hex')
  else return buf
}

function isHexString (str, length) {
  if (!isString(str)) return false
  if (length && (str.length !== length)) return false
  return RegExp('[0-9a-fA-F]+').test(str)
}

function removeEmpty (obj) {
  return Object.keys(obj)
    .filter(k => obj[k] != null)
    .reduce((newObj, k) => {
      return typeof obj[k] === "object"
        ? { ...newObj, [k]: removeEmpty(obj[k]) }
        : { ...newObj, [k]: obj[k] }
    }, {})
}

module.exports = {
  isDevelopment: () => process.env.NODE_ENV === 'development',
  isProduction: () => process.env.NODE_ENV === 'production',
  printAppInfo,
  untilde,
  hex,
  isHexString,
  removeEmpty
}
