const debug = require('debug')('cobox-hub')
const SpaceDecorator = require('./decorators/space')

function logResource (record) {
  var decorator = SpaceDecorator(record)
  var resource = decorator.toJSON({ secure: true })
  debug(`active ${record.constructor.name}: ${JSON.stringify(resource, null, 2)}`)
  debug(`swarming:`, record._swarm ? true : false)
}

module.exports = {
  logResource
}
