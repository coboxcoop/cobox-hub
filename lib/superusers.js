const debug = require('debug')('cobox-server')
const crypto = require('cobox-crypto')
const path = require('path')
const Repository = require('@coboxcoop/repository')
const Superuser = require('../app/models/superuser')
const { deriveFromStoredParent } = require('cobox-keys')
const { isDevelopment } = require('../util')
const { logResource } = require('./util')
const { assign } = Object

module.exports = (storage, opts = {}) => {
  const namespace = path.join(storage, 'superusers')
  const deriveKeyPair = deriveFromStoredParent(storage, crypto.keyPair)
  const createSuperuser = Superuser(namespace, assign({ deriveKeyPair, opts }))
  const repository = Repository(namespace, createSuperuser)
  if (isDevelopment()) repository.on('entry', logResource)
  return { store: repository }
}
