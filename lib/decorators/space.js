const debug = require('debug')('cobox-hub')
const { hex, removeEmpty } = require('../../util')
const { loadKey } = require('cobox-keys')

const SpaceDecorator = module.exports = (space) => ({
  toJSON: (opts = {}) => removeEmpty({
    name: space.name,
    address: hex(space.address),
    discoveryKey: space.discoveryKey ? hex(space.discoveryKey) : null,
    encryptionKey: opts.secure ? hex(loadKey(space.path, 'encryption_key')) : null,
    size: space.bytesUsed()
  }),
  export: () => ({
    name: space.name,
    address: hex(space.address),
    encryptionKey: hex(loadKey(space.path, 'encryption_key'))
  })
})
