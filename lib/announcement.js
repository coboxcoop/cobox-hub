const crypto = require('cobox-crypto')
const createBroadcastStream = require('broadcast-stream')
const { EventEmitter } = require('events')
const debug = require('debug')('cobox-hub')
const { hex } = require('../util')

const Announcement = module.exports = (opts = {}) => {
  const id = hex(crypto.randomBytes(2))
  const port = opts.port || 8999
  const interval = opts.interval || 1000
  const onconnection = opts.onconnection || log
  let stream = null
  let end = noop
  let destroy = noop

  return {
    start,
    stop
  }

  function start (payload) {
    if (stream) return debug('already announcing')
    debug(`${id} announcing ${payload()} on port ${port}`)
    stream = createBroadcastStream(port)
    destroy = setInterval(() => {
      if (stream) stream.write(payload())
    }, interval)

    stream.on('data', onconnection)

    end = () => {
      debug(`${id} announcement shutting down`)
      clearInterval(destroy)
      stream.end()
    }

    return { port, interval }
  }

  function stop () {
    if (!end) return debug('already stopped')
    end()
    debug(`${id} announcement stopped`)
    end = noop
    stream = null
    return {}
  }

  function log (msg) {
    return debug(`${id}`, msg)
  }
}

function noop () {}

module.exports = Announcement
