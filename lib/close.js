const debug = require('debug')('cobox-hub')
const thunky = require('thunky')
const capture = require('capture-exit')

module.exports = function close (app, opts) {
  const api = app.get('api')

  return thunky(function (cb = noop) {
    let pending = 2

    api.superusers.store.close(next)
    api.replicators.store.close(next)

    function next (err) {
      if (err) {
        pending = Infinity
        return cb(err)
      }
      if (!--pending) return cb(null, done)
    }

    function done () {
      app.server.forceShutdown((err) => {
        debug('closed: server', err || '')
        console.log('\n...by block by block, blocks all the way down')
        capture.offExit(api.shutdown)
        capture.releaseExit()
        return process.exit(0)
      })
    }
  })
}
