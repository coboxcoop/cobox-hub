const fs = require('fs')
const constants = require('cobox-constants')
const yargs = require('yargs')
const findUp = require('find-up')
const YAML = require('js-yaml')
const config = require('@coboxcoop/config')

const App = require('./index')
const options = require('./bin/lib/options')
const { printAppInfo } = require('./util')

const args = yargs
  .config(config.load())
  .options(options)
  .argv

if (require.main === module) return run(args)
else module.exports = run

function run (opts) {
  const app = App(opts)

  app.start(() => {
    var hostname = app.get('hostname')
    var port = app.get('port')
    var udpPort = app.get('udpPort')
    var storage = app.get('storage')

    const info = { hostname, port, udpPort, storage }
    return printAppInfo(info)
  })
}
