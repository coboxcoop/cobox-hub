const { body, sanitize } = require('express-validator')
const crypto = require('cobox-crypto')
const assert = require('assert')
const {
  SpaceByNameChain,
  SpaceByAddressChain
} = require('../chains')


module.exports = (api) => {
  const replicatorByNameChain = SpaceByNameChain(api.replicators.store)
  const replicatorByAddressChain = SpaceByAddressChain(api.replicators.store)

  return {
    create: [
      replicatorByNameChain(),
      replicatorByAddressChain()
    ]
  }
}
