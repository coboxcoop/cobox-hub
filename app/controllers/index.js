const ApplicationController = module.exports = (api) => ({
  commands: require('./commands')(api),
  replicators: require('./replicators')(api),
  keys: require('./keys')(api),
  profile: require('./profile')(api),
  system: require('./system')(api)
})
