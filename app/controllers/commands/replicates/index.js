const through = require('through2')
const { validators } = require('cobox-schemas')
const set = require('lodash.set')
const sortBy = require('lodash.sortby')
const transform = require('lodash.transform')
const collect = require('collect-stream')
const crypto = require('cobox-crypto')
const debug = require('debug')('cobox-hub')
const { assign } = Object
const pick = require('lodash.pick')

const isReplicate = validators.command.replicate
const isUnreplicate = validators.command.unreplicate

const REPLICATE = 'command/replicate'
const UNREPLICATE = 'command/unreplicate'

const NotFoundError = require('../../../../lib/errors/not-found')
const filter = require('../../../helpers/filter')
const resourceParams = require('../../../helpers/resource-params')
const genId = require('../../../helpers/gen-id')
const { hex } = require('../../../../util')

const ReplicatesController = module.exports = (api) => {
  const state = api.store.commands.replicates
  // {
  //   [address]: {
  //     [commandId]: {
  //       name,
  //       address,
  //       timestamp,
  //       command
  //     }
  //   }
  // }

  return {
    index,
    live,
    command
  }

  // -------------------- Controller Actions -------------------- //

  function live (params, opts = {}) {
    return api.superuser.log
      .read({ query: query(), live: true, old: false })
      .pipe(filter(isValid))
      .pipe(reduce())
  }

  async function index (params, opts = {}) {
    return await new Promise((resolve, reject) => {
      const commands = api.superuser.log
        .read({ query: query() })
        .pipe(filter(isValid))
        .pipe(reduce())

      collect(commands, (err, msgs) => {
        if (err) return reject(err)
        return resolve(msgs)
      })
    })
  }

  async function command (params, opts = {}) {
    await index()
    ondata()
    live().on('data', ondata)

    async function ondata (record) {
      const records = reduceState()
      if (!records) return false
      let actions = await Promise.all(records.map(findOrCreateReplicator))

      return await Promise.all(actions.map(async (action) => {
        await execute(action.command, action.params)
      }))
    }
  }

  // --------------------- Helpers ---------------------- //

  function query (params = {}, opts = {}) {
    const $map = opts.$map || {}
    const $reduce = opts.$reduce || {}
    const value = Object.assign(params, { type: { $in: [REPLICATE, UNREPLICATE] },  timestamp: { $gt: 0 } })
    return [{ $filter: { value }, $map, $reduce }]
  }


  function isValid (msg) {
    return isReplicate(msg.value) || isUnreplicate(msg.value)
  }

  function reduce () {
    return through.obj(function (msg, enc, next) {
      const { timestamp, type } = msg.value
      const { name, address } = msg.value.content
      const details = { name, address, command: type, timestamp }
      const id = hex(genId(msg))
      set(state, [address, id], details)
      this.push(details)
      next()
    })
  }

  async function findOrCreateReplicator (record) {
    const params = resourceParams(record)
    const details = record.commands[record.commands.length - 1]
    const command = details.command

    let replicator

    try {
      replicator = await api.controllers.replicators.show(params)
      return { params, command }
    } catch (err) {
      if (!err.notFound) throw err
      replicator = await api.controllers.replicators.create(params)
      return { params, command }
    }
  }

  function reduceState () {
    return transform(state, (acc, records, address) => {
      let values = Object.values(records)
      let last = values[values.length-1]
      acc.push({
        address,
        name: last.name,
        commands: transform(records, (acc, details, id) => {
          acc.push(assign({ id }, {
            timestamp: details.timestamp,
            command: details.command
          }))
        }, [])
      })
    }, [])
  }

  async function execute (command, params) {
    var mappings = Mappings()
    var fn = mappings[command]
    if (fn) return await fn(params, opts = {})
    else throw new NotFoundError('invalid command')
  }

  function Mappings () {
    return {
      [REPLICATE]: (params, opts) => api.controllers.replicators.connections.create({}, params),
      [UNREPLICATE]: api.controllers.replicators.connections.destroy
    }
  }
}
