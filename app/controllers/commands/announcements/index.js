const through = require('through2')
const { encodings, validators } = require('cobox-schemas')
const { loadKey } = require('cobox-keys')
const set = require('lodash.set')
const sortBy = require('lodash.sortby')
const transform = require('lodash.transform')
const collect = require('collect-stream')
const { assign } = Object
const debug = require('debug')('cobox-hub:announcements')

const NotFoundError = require('../../../../lib/errors/not-found')
const Announcement = require('../../../../lib/announcement')

const Broadcast = encodings.devices.broadcast
const isAnnounce = validators.command.announce
const isHide = validators.command.hide

const filter = require('../../../helpers/filter')
const genId = require('../../../helpers/gen-id')
const { hex } = require('../../../../util')

const ANNOUNCE = 'command/announce'
const HIDE = 'command/hide'

const AnnouncementsController = module.exports = (api) => {
  const announcement = Announcement({
    port: api.udpPort
  })

  const state = api.store.commands.announcements
  // {
  //   [commandId]: {
  //     timestamp,
  //     command
  //   }
  // }

  return {
    index,
    live,
    create,
    destroy,
    command
  }

  // -------------------- Controller Actions -------------------- //

  function live (params, opts = {}) {
    return api.superuser.log
      .read({ query: query(), live: true, old: false })
      .pipe(filter(isValid))
      .pipe(reduce())
  }

  async function index (params, opts = {}) {
    return await new Promise((resolve, reject) => {
      const commands = api.superuser.log
        .read({ query: query() })
        .pipe(filter(isValid))
        .pipe(reduce())

      collect(commands, (err, msgs) => {
        if (err) return reject(err)
        return resolve(msgs)
      })
    })
  }

  async function command (params, opts) {
    await index()
    const commands = reduceState()
    done(commands[commands.length - 1])
    live().on('data', done)

    async function done (msg) {
      if (!msg) return await create()
      if (msg.sync) return
      try { return await execute(msg.command, msg) }
      catch (err) { return debug(err) }
    }
  }

  function create (params, opts) {
    const buildPayload = () => Broadcast.encode({
      type: 'devices/broadcast',
      timestamp: Date.now(),
      author: hex(api.profile.publicKey),
      content: {
        address: hex(api.superuser.address),
        encryptionKey: hex(loadKey(api.superuser.path, 'encryption_key')),
        broadcasting: true
      }
    })

    return announcement.start(buildPayload)
  }

  function destroy (params, opts) {
    return announcement.stop()
  }

  // --------------------- Helpers ---------------------- //

  function query (params = {}, opts = {}) {
    const $map = opts.$map || {}
    const $reduce = opts.$reduce || {}
    const value = Object.assign(params, { type: { $in: [ANNOUNCE, HIDE] },  timestamp: { $gt: 0 } })
    return [{ $filter: { value }, $map, $reduce }]
  }

  function isValid (msg) {
    return isAnnounce(msg.value) || isHide(msg.value)
  }

  function reduce () {
    return through.obj(function (msg, enc, next) {
      const { timestamp, type: command } = msg.value
      const details = { command, timestamp }
      const id = hex(genId(msg))
      set(state, [id], details)
      this.push(details)
      next()
    })
  }

  function reduceState () {
    return transform(state, (acc, record, id) => {
      acc.push(assign({ id }, record))
    }, [])
  }

  async function execute (command, params) {
    var mappings = Mappings()
    var fn = mappings[command]
    if (fn) return await fn(params, opts = {})
    else throw new NotFoundError('invalid command')
  }

  function Mappings () {
    return {
      [ANNOUNCE]: create,
      [HIDE]: destroy
    }
  }
}
