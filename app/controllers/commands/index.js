const CommandsController = module.exports = (api) => ({
  announcements: require('./announcements')(api),
  replicates: require('./replicates')(api)
})
