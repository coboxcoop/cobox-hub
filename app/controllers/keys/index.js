const debug = require('debug')('cobox-hub')
const { loadKey } = require('cobox-keys')
const SpaceDecorator = require('../../../lib/decorators/space')
const { hex } = require('../../../util')

const PARENT_KEY = 'parent_key'

const KeysController = module.exports = (api) => {
  const storage = api.settings.storage
  const Replicator = api.replicators.store

  const controller = {
    index,
    create
  }

  return controller

  // -------------------- Controller Actions -------------------- //

  // GET /api/keys
  async function index (params, opts = {}) {
    let replicators = await Replicator.all()

    replicators = replicators
      .map(SpaceDecorator)
      .map((decorator) => decorator.export())

    return {
      parentKey: hex(loadKey(storage, PARENT_KEY)),
      admin: SpaceDecorator(api.superuser).export(),
      replicators
    }
  }

  // POST /api/keys
  async function create (params, opts = {}) {
    throw new Error('route not yet implemented')
  }
}
