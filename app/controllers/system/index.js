const debug = require('debug')('cobox-server')
const pick = require('lodash.pick')

const SystemController = module.exports = (api) => {
  const controller = {
    routes,
    show
  }

  return controller

  // -------------------- Controller Actions -------------------- //

  // GET /api/system/routes
  function routes (params, opts) {
    const router = params.router
    return []
      .concat
      .apply([], router.stack.map((layer) => recurse(layer)))
      .filter(Boolean)
  }

  // GET /api/system
  async function show (params, opts = {}) {
    const system = systemParams(api.system)
    return system
  }
}

function systemParams (params) {
  return pick(params, [
    'name',
    'description',
    'version',
    'author',
    'license',
    'bugs'
  ])
}

function recurse (layer) {
  if (!layer.route) {
    var match = layer.regexp.toString().match(/\/\^\\(\/\w+)/)
    if (!match) return
    var path = match[1]

    var stack = layer.handle.stack
    var nestedRoutes = stack.map((l) => {
      var rs = recurse(l)
        .map((route) => {
          route.path = path.concat(route.path)
          return route
        })

      return rs
    })
    var rs = [].concat.apply([], nestedRoutes)
    return rs
  } else {
    var path = layer.route.path
    if (!path) return
    var rs = layer.route.stack.reduce((acc, l) => {
      acc.push({ path, method: l.method })
      return acc
    }, [])
    return rs
  }
}
