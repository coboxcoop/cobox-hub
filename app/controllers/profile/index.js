const debug = require('debug')('cobox-server')
const crypto = require('cobox-crypto')
const ProfileDecorator = require('../../../lib/decorators/profile')

const ProfileController = module.exports = (api) => {
  const profile = api.profile

  const controller = {
    show,
  }

  return controller

  // -------------------- Controller Actions -------------------- //

  // GET /api/profile
  async function show (params, opts = {}) {
    return ProfileDecorator(profile).toJSON()
  }
}
