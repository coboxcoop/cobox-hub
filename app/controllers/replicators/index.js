const debug = require('debug')('cobox-hub:replicators')
const assert = require('assert')
const resourceParams = require('../../helpers/resource-params')
const SpaceDecorator = require('../../../lib/decorators/space')
const ConnectionsController = require('./connections')
const { encodings, validators } = require('cobox-schemas')

const Replicate = encodings.command.replicate
const Unreplicate = encodings.command.unreplicate
const isReplicate = validators.command.replicate
const isUnreplicate = validators.command.unreplicate
const { assign } = Object

const { hex } = require('../../../util')

const REPLICATE = 'command/replicate'
const UNREPLICATE = 'command/unreplicate'

const ReplicatorsController = module.exports = (api) => {
  const Replicator = api.replicators.store

  const controller = {
    index,
    show,
    create
  }

  const children = {
    connections: ConnectionsController(api, controller)
  }

  return assign(controller, children)

  // -------------------- Controller Actions -------------------- //

  // GET /api/replicators
  async function index (params, opts = {}) {
    let replicators = await Replicator.all()

    if (!opts.decorate) return replicators

    return replicators
      .map(SpaceDecorator)
      .map((decorator) => decorator.toJSON())
  }

  // GET /api/replicators/:id
  async function show (params, opts = {}) {
    let replicator = await Replicator.findBy(resourceParams(params))
    assert(replicator, 'does not exist')

    if (!opts.decorate) return replicator

    let decorator = SpaceDecorator(replicator)
    return decorator.toJSON()
  }

  // POST /api/replicators
  async function create (params, opts = {}) {
    let replicator = await Replicator.create(resourceParams(params))

    assert(replicator, 'failed to create')

    let msg = {
      type: REPLICATE,
      timestamp: Date.now(),
      author: hex(api.profile.publicKey),
      content: replicator.attributes
    }

    assert(isReplicate(msg), 'invalid parameters')

    await api.superuser.log.publish(Replicate.encode(msg))

    if (!opts.decorate) return replicator
    let decorator = SpaceDecorator(replicator)
    return decorator.toJSON()
  }

  // DELETE /api/replicators
  async function destroy (params, opts = {}) {
    return console.log("not yet implemented")
    let replicator = await Replicator.destroy(resourceParams(params))

    let msg = {
      type: UNREPLICATE,
      timestamp: Date.now(),
      author: hex(api.profile.publicKey),
      content: replicator.attributes
    }

    assert(isUnreplicate(msg), 'invalid parameters')

    await api.superuser.log.publish(Unreplicate.encode(msg))

    if (!opts.decorate) return replicator
    let decorator = SpaceDecorator(replicator)
    return decorator.toJSON()
  }
}
