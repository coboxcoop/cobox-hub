const express = require('express')
const toHTTP = require('../../middleware/to-http')

module.exports = (app) =>  {
  const router = express.Router()

  router.use('/announcements', require('./announcements')(app))

  return router
}
