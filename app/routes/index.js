const express = require('express')
const toHTTP = require('../middleware/to-http')
const debug = require('debug')('cobox-hub')

const ApplicationController = require('../controllers')
const ApplicationValidator = require('../validators')

module.exports = function Routes (api) {
  const router = express.Router()

  const app = {
    controllers: api.controllers,
    validators: ApplicationValidator(api)
  }

  router.use('/commands', require('./commands')(app))
  router.use('/replicators', require('./replicators')(app))
  router.use('/keys', require('./keys')(app))
  router.use('/profile', require('./profile')(app))
  router.use('/system', require('./system')(app))

  router.get('/system/routes', (req, res) => {
    let routes = app.controllers.system.routes({ router })

    return res
      .status(200)
      .json(routes)
  })

  router.get('/stop', (req, res) => {
    api.shutdown()
      .then((done) => {
        res.status(200)
        return done()
      }).catch((err, done) => {
        debug(err)
        res.status(500)
        return done()
      })
  })

  return router
}

