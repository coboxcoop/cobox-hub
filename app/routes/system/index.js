const express = require('express')
const toHTTP = require('../../middleware/to-http')

const KeyRoutes = module.exports = ({ controllers, validators }) => {
  const router = express.Router()

  router.get('/', toHTTP(controllers.system.show))

  return router
}
