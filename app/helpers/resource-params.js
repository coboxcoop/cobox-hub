const pick = require('lodash.pick')

module.exports = function resourceParams (params) {
  return pick(params, ['name', 'address'])
}
