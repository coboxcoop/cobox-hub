const debug = require('debug')('cobox-hub')
const through = require('through2')

module.exports = function filter (fn) {
  return through.obj(function (msg, enc, next) {
    if (fn(msg)) this.push(msg)
    next()
  })
}

