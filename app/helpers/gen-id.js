const crypto = require('cobox-crypto')

module.exports = function genId (msg) {
  return crypto.genericHash(Buffer.from([msg.key, '@', msg.seq].join('')), null, 16)
}
