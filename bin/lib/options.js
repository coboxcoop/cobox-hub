const constants = require('cobox-constants')

module.exports = {
  port: {
    alias: 'p',
    number: true,
    describe: 'hub server port',
    default: 9111
  },
  udpPort: {
    alias: 'b',
    number: true,
    describe: 'udp packet port',
    default: 8999
  },
  hostname: {
    describe: 'hostname',
    default: 'localhost'
  },
  storage: {
    alias: 's',
    type: 'string',
    describe: 'storage path of the application',
    default: constants.hubStorage
  },
  dev: {
    describe: 'start in developer mode'
  }
}
